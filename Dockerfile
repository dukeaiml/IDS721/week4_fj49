# Use an official Rust image as the builder stage
FROM rust:1.75 as builder

# Create a new empty shell project
RUN USER=root cargo new --name actix_web_app_fj49 actix_web_app_fj49
WORKDIR /actix_web_app_fj49

# Copy the manifests
COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml

# Get the dependencies cached
RUN cargo build --release

# Remove the dummy source files
RUN rm src/*.rs

# Copy source code
COPY ./src ./src

# Build for release
RUN rm ./target/release/deps/actix_web_app_fj49*

RUN cargo build --release

# Final stage
FROM debian:bookworm-slim

# Set the working directory
WORKDIR /usr/src/actix_web_app_fj49

# Copy the compiled binary from the builder stage
COPY --from=builder /actix_web_app_fj49/target/release/actix_web_app_fj49 .

# Set execute permission
RUN chmod +x ./actix_web_app_fj49

# Expose port 8080
EXPOSE 8080

# Run the binary
CMD ["./actix_web_app_fj49"]
