# FJ49 - Rust Axum Mini Project 4

### Containerizing a Rust Actix Web Service
This project aims to containerize a Rust Actix web service that generates the factors of a given integer by defining an endpoint in the webservice for users to give a large integer. Basic error handling is also done.

### Packages and Dependencies
To install Docker:

1 - Go to the official Docker website: [Docker Official Website.](https://docs.docker.com/desktop/)

2 - Follow the instructions provided to download and install Docker for your operating system.

### Build and Deploy the Project

Follow these steps to build and deploy the project:

1 - Run the following command to create a new blank Rust project:

    cargo new actix_web_app_fj49

2 - Update the code in the /src/main.rs file to add functionalities to the Rust function or if you want to change anything

3 - Add dependencies to the Cargo.toml file.

    actix-web = "4"

4 - Run the following command to test whether the main function works locally:

    cargo run

5 - Open the Docker console and ensure that Docker Desktop is running.

6 - Write the Dockerfile to specify the steps including FROM, WORKDIR, USER, COPY, RUN, EXPOSE, and CMD.

7 - Build the Docker image by running the following command in the terminal:

    docker build -t actix_web_app_fj49 .

8 - Run the Docker container by executing the following command:

    docker run -p 8080:8080 actix_web_app_fj49

9 - Running the Container
After building the Docker image, run the container using the following command:

    docker run -p 8080:8080 actix_web_app_fj49

10 - Access the Actix web service by navigating to http://localhost:8080 in your web browser.


### Results

#### Web app running

![Screenshot](screenshots/app_running.png)


#### Docker container building

![Screenshot](screenshots/docker_building.png)


#### Docker Image on docker desktop

![Screenshot](screenshots/docker_image.png)


#### Container running

![Screenshot](screenshots/container.png)



