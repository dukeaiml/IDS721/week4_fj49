use actix_web::{web, App, HttpServer, HttpResponse, Responder};

fn factorize(number: u64) -> Vec<u64> {
    let mut factors = Vec::new();
    let mut n = number;
    let mut divisor = 2;

    while n > 1 {
        while n % divisor == 0 {
            factors.push(divisor);
            n /= divisor;
        }
        divisor += 1;
    }

    factors
}

async fn factorize_number(input: web::Path<String>) -> impl Responder {
    if let Ok(number) = input.parse::<u64>() {
        let factors = factorize(number);
        HttpResponse::Ok().body(format!("Factors of {}: {:?}", number, factors))
    } else {
        HttpResponse::BadRequest().body("Invalid input. Please provide a valid number.")
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/factorize/{number}", web::get().to(factorize_number))
    })
    .bind("127.0.0.1:8080")?
    .run()
    .await
}
